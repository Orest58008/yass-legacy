use std::env;
use std::fs;
use std::collections::HashMap;
fn main() {
   //blank character to use in unwrap_or()
    let blank = String::from("");

   //environment variables
    let mut username = String::new();
    match env::var_os("USER") { Some(val) => username = val.into_string().unwrap(), None => username = username}
    
   //reading files
    let hostname = fs::read_to_string("/etc/hostname").unwrap_or(blank.clone()).replace('\n', "");
    
   //reading os-release
    let os_release = read_to_map("/etc/os-release", '=');

    let os_name;
    match os_release.get("NAME") {
        Some(name) => os_name = name.trim_matches('\"'),
        None => os_name = ""
    }
    
    let os_version;
    match os_release.get("VERSION") {
        Some(version) => os_version = version.trim_matches('\"'),
        None => os_version = ""
    }

   //reading meminfo
    let meminfo = read_to_map("/proc/meminfo", ':');

    let total_memory;
    match meminfo.get("MemTotal") {
        Some(memtotal) => total_memory = memtotal.replace(" kB", "").parse::<u32>().unwrap() / 1024,
        None => total_memory = 0
    }

    let free_memory;
    match meminfo.get("MemFree") {
        Some(memfree) => free_memory = memfree.replace(" kB", "").parse::<u32>().unwrap() / 1024,
        None => free_memory = 0
    }

    let used_memory = total_memory - free_memory;

    let total_swap;
    match meminfo.get("SwapTotal") {
        Some(swapfree) => total_swap = swapfree.replace(" kB", "").parse::<u32>().unwrap() / 1024,
        None => total_swap = 0
    }
    
    let free_swap;
    match meminfo.get("SwapFree") {
        Some(swapfree) => free_swap = swapfree.replace(" kB", "").parse::<u32>().unwrap() / 1024,
        None => free_swap = 0
    }

    let used_swap = total_swap - free_swap;

   //reading uptime
    let uptime_temp = fs::read_to_string("/proc/uptime").unwrap_or(blank.clone());
    let uptime_vec: Vec<&str> = uptime_temp.split(' ').collect();
    let uptime: f64 = uptime_vec[0].parse().unwrap();
    let uptime_hrs = (uptime / 3600.0) as u16;
    let uptime_mins = (uptime - (uptime_hrs * 3600) as f64) as u16 / 60;
    let uptime_secs = (uptime - (uptime_hrs * 3600) as f64 - (uptime_mins * 60) as f64) as u16;

   //reading kernel version
    let kernel_version = fs::read_to_string("/proc/sys/kernel/osrelease").unwrap_or(blank.clone()).replace('\n', "");

   //left-side art
    let art = "
▙    
▜▙   
 ▜▙  
  ▜▙ 
  ▟▛ 
 ▟▛  
▟▛   
▛    ";
    let art_lines: Vec<&str> = art.lines().collect();
    let color = distro_color(&os_name).to_string() + "m";
    let bold = "\u{001b}[1m";
    let underline = "\u{001b}[4m";
    let clear = "\u{001b}[0m";

   //print info
    println!("{color}{}{clear}", art_lines[1]);
    println!("{color}{} {bold}{underline}{username}{clear}@{color}{underline}{bold}{hostname}{clear}", art_lines[2]);
    println!("{color}{} {bold}ОС:          {clear}{os_name} {os_version}", art_lines[3]);
    println!("{color}{} {bold}Кернель:     {clear}{kernel_version}", art_lines[4]);
    println!("{color}{} {bold}RAM:         {clear}{used_memory}/{total_memory} MiB", art_lines[5]);
    println!("{color}{} {bold}Swap:        {clear}{used_swap}/{total_swap} MiB", art_lines[6]);
    println!("{color}{} {bold}Час роботи:  {clear}{uptime_hrs}:{uptime_mins}:{uptime_secs}", art_lines[7]);
    println!("{color}{}{clear}", art_lines[8]);
}

fn distro_color(distro: &str) -> &str {
    match distro {
        "Ubuntu" => "\u{001b}[31",       //red
        "Debian" => "\u{001b}[31",       //red
        "Gentoo" => "\u{001b}[35",       //magenta
        "Fedora Linux" => "\u{001b}[34", //blue
        "OpenSUSE" => "\u{001b}[32",     //green
        "Linux Mint" => "\u{001b}[32",   //green
        "Arch Linux" => "\u{001b}[36",   //cyan
        "Manjaro" => "\u{001b}[32",      //green
        "Slackware" => "\u{001b}[34",    //blue
        _ => "\u{001b}[0"                //clear
    }
}

fn read_to_map(path: &str, keyvalue_splitter: char) -> HashMap<String, String> {
    let temp_str = fs::read_to_string(path).unwrap_or(String::from(""));
    let mut temp: Vec<&str> = temp_str.split('\n').collect();
    temp = temp[0..temp.len() - 2].to_vec();
    let mut temp_map = HashMap::new();
    let mut temp_entry: Vec<&str>;
    for e in temp {
        temp_entry = e.split(keyvalue_splitter).collect();
        temp_map.insert(String::from(temp_entry[0]), String::from(temp_entry[1].trim()));
    }
    temp_map
}
